MUITO IMPORTANTE:
     - Quando vocês colocarem plugins novos, no final da instrução que adiciona o plugin tem que colocar o comando "--save" sem as aspas.
     - Dependendo do programa que vocês usarem para codificar ele pode criar pastas desnecessárias para as outras pessoas do grupo, então adicione elas no arquivo .gitignore.