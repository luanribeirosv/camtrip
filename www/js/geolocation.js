
/**
 * Função para pegar a posição atual do usuário.
 * 
 * Recebe como parâmetro uma função que retornará
 * a latitude e longitude passadas por parâmetro.
 */
function getCurrentPosition(functionReturn) {

    navigator.geolocation.getCurrentPosition(
        function(position) {
            functionReturn(position.coords.latitude,
                            position.coords.longitude);
        }, 
        function(error) {
            alert("Falha ao resolver localização - o GPS está ativo?");
        });

}