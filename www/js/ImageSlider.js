function ImageSlider() {

  // Properties
  this.sliderId = undefined;
  this.sliderDomObj = undefined;
  this.bxSliderObj = undefined;
  this.currentPhoto = undefined;

  // Methods
  this.init = function(sliderId, options) {
    this.sliderId = sliderId || undefined;
    options = options || {};

    if (this.sliderId) {
      this.sliderDomObj = document.getElementById(this.sliderId) || undefined;

      if (this.sliderDomObj)
        this.bxSliderObj = $(this.sliderDomObj).bxSlider(options);

    }
  };

  this.addImages = function(photos) {
    for (var i = 0; i < photos.length; ++i) {
      var photo = photos[i];
      var path = photo.path;

      if (this.bxSliderObj && path) {
        var img = document.createElement("img");
        img.src = path;

        var li = document.createElement("li");
        li.appendChild(img);

        this.sliderDomObj.appendChild(li);

      }
    }

    this.bxSliderObj.reloadSlider();
  };

  this.goToSlide = function(index) {
    if (this.bxSliderObj) {
      this.bxSliderObj.goToSlide(index);
    }
  };

  this.getCurrentSlide = function() {
    if (this.bxSliderObj)
      return this.bxSliderObj.getCurrentSlide();
    else {
      return undefined;
    }
  };

  this.reload = function(addAllImagesToSliderFunction) {
    $(this.sliderDomObj).empty();
    addAllImagesToSliderFunction(this);
  };

}
