
//database variables
var db;
var db_name = "camtrip_db";
var db_version = "1.0";
var db_description = "CamTrip DataBase";
var db_size = 2 * 1024 * 1024;
/*
Contém a tablea PHOTOS com os seguintes atributos:
path (unique); latitude; longitude.
*/

function manageDatabase() {
  db = window.openDatabase(db_name, db_version, db_description, db_size);
  db.transaction(function (tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS PHOTOS (path unique, latitude, longitude, date)');
  });
}

function saveImageToDataBase(filePath, callback) {

  var date = new Date();

  getCurrentPosition(function(latitude, longitude) {
    db.transaction(function (tx) {
      tx.executeSql('INSERT INTO PHOTOS (path, latitude, longitude, date) VALUES (?, ?, ?, ?)', [filePath, latitude, longitude, date], function() {
        if (callback)
          callback();
      });
    });
  });

}

function deleteImageFromDataBase(filePath, callback) {
  db.transaction(function(tx) {
    tx.executeSql('DELETE FROM PHOTOS WHERE path = ?', [filePath], function() {
      // alert("The Image was deleted!!");
      callback();
    }, function(error) {
      alert("Failed on deleting photo");
    });
  });
}

/**
* Esta função chama um callback que deve esperar como parametro os resultados,
* contendo o path, latitude, longitude.
*/
function getAllImagesDB(callback) {
  db.readTransaction(function (tx) {
    tx.executeSql("SELECT * FROM PHOTOS", [], function (tx, results) {
      var photos = new Array();
      for (var i = 0; i < results.rows.length; i++) {
        photos.push(results.rows.item(i));
      }

      callback(photos);
    }, function (error) {
      alert("error executing SELECT query.");
    });
  });
}

/**
* Esta função chama um callback que deve esperar como parametro uma foto, estando undefined no caso de não haver,
* contendo o path, latitude, longitude.
*/
function getImageDB(index, callback) {
  db.readTransaction(function (tx) {
    tx.executeSql("SELECT * FROM PHOTOS LIMIT 1 OFFSET ?", [index], function (tx, results) {

      if (results.rows.length != 1)
        callback(undefined);

      var photo = results.rows.item(0);

      callback(photo);
    }, function (error) {
      alert("error executing SELECT query.");
    });
  });
}
