
var map;
var autocomplete;

var txtEndereco;
var btnBuscarEndereco;

function viewMap() {

    txtEndereco = document.getElementById('txtEndereco');

    btnBuscarEndereco = document.getElementById('btnBuscarEndereco');
    btnBuscarEndereco.addEventListener("click", buscarEndereco, false);

    window.setTimeout(getMapLocation, 500);

    window.location.href = '#pageMap';
}

function getMapLocation() {

    getCurrentPosition(function (latitude, longitude) {
        getMap(latitude, longitude);

        getPoints();
    });
}

function getMap(latitude, longitude) {
    var mapOptions = {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map
        (document.getElementById("map"), mapOptions);

    autocomplete = new google.maps.places.Autocomplete(txtEndereco);
    autocomplete.bindTo('bounds', map);
}

function getPoints() {

    getAllImagesDB(function (result) {
        var markers = new Array();

        for (i = 0; i < result.length; i++) {

            var icon = {
                url: "img/marcador.png",
                scaledSize: new google.maps.Size(50, 50)
            };

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(result[i].latitude, result[i].longitude),
                map: map,
                icon: icon
            });

            markers.push(marker);

            drawInfoBox(marker, result[i].path, i);
        }

        var markerClusterer = new MarkerClusterer(map, markers, { maxZoom: 15 });

        drawPath(markers);
    });
}

function drawPath(markers) {
    var coordinates = new Array();

    for (i = 0; i < markers.length; i++) {
        coordinates.push(markers[i].position);
    }

    var path = new google.maps.Polyline({
        path: coordinates,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });

    path.setMap(map);
}

function viewImageSlider(index) {
	//como alterar a img do slider sem que o slider trave na img?
    window.location.href = '#pageIndex';
    setTimeout(function() {imageSlider.goToSlide(index)}, 500);
}

function drawInfoBox(marker, image, index) {
    var myOptions = {
        content: "<img onclick=\"viewImageSlider(" + index + ")\" style=\"width: 100px; height: 100px;\" src=\"" + image + "\"></img>"
    };

    var infoBox = new InfoBox(myOptions);
    infoBox.marker = marker;

    infoBox.listener = google.maps.event.addListener(marker, 'click', function (e) {
        infoBox.open(map, marker);
    });
}

function buscarEndereco() {
    map.setCenter(autocomplete.getPlace().geometry.location);
    map.setZoom(13);
}