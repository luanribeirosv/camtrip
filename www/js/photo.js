function getImageURI(imageURI) {
  movePic(imageURI);
}

function movePic(file){
  window.resolveLocalFileSystemURI(file, resolveOnSuccess, function(){alert("Falha ao resolver arquivo!");});
}

function resolveOnSuccess(entry){
  var myFolderApp = cordova.file.applicationStorageDirectory;

  window.resolveLocalFileSystemURL(cordova.file.applicationStorageDirectory, function (dirEntry) {
    entry.moveTo(dirEntry, "camtrip_"+entry.name,  successMove, resOnError);
  }, resOnError);
}

function deletePic(path, callback) {
  window.resolveLocalFileSystemURI("file://" + path, function(fileEntry) {
    fileEntry.remove(function() {
      // The file has been removed succesfully
      deleteImageFromDataBase(path, callback);
    }, function(error) {
      // Error deleting the file
      alert("We got some error deleting the Pic");
    }, function() {
      // The file doesn't exist
      alert("The file don't exist!! WHAT!!!???");
    });
  }, function(error) {
    alert("Falha ao resolver arquivo!: " + error);
  });
}

function successMove(entry){
  saveImageToDataBase(entry.fullPath, function() {
    imageSlider.reload(addAllImagesToSlider);
  });
}

function resOnError(error) {
  alert(error.code);
}
