// Image Slider obj
var imageSlider = new ImageSlider();

/*Functions called by page buttons*/
function takePicture() {
  navigator.camera.getPicture(getImageURI, onFail, {
    econdingType: Camera.EncodingType.JPEG,
    saveToPhotoAlbum: false,
    destinationType: Camera.DestinationType.FILE_URI,
  });
}

function onFail() {
  alert("Falha ao tirar foto.");
}

function addAllImagesToSlider(imageSlider) {
  getAllImagesDB(function(photos) {
    imageSlider.addImages(photos);
  });
}

function updatePhotoData(imageSlider) {
  var currentImageDate1 = document.getElementById("currentImageDate1");
  var currentImageLatLong1 = document.getElementById("currentImageLatLong1");
  var currentImageDate = document.getElementById("currentImageDate");
  var currentImageLatLong = document.getElementById("currentImageLatLong");
  var currentImageDelete = document.getElementById("currentImageDelete");

  var photo = imageSlider.currentPhoto;

  if (!photo)
    return;

  var latlng = new google.maps.LatLng(photo.latitude, photo.longitude);
  var geocoder = geocoder = new google.maps.Geocoder();
  geocoder.geocode({ 'latLng': latlng }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
          if (results[1]) {
              currentImageLatLong.innerHTML = results[1].formatted_address;
          }
      }
  });

  currentImageDate1.innerHTML = "Tirada em:";
  currentImageLatLong1.innerHTML = "Endereço:";
  currentImageDate.innerHTML = moment(photo.date).format("DD/MM/YYYY, hh:mm:ss a");
  currentImageDelete.onclick = function () {
    deletePic(photo.path, function() {
      imageSlider.reload(addAllImagesToSlider);
    });
  };
}

//Application
var app = {
  // Application Constructor
  initialize: function() {
    document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
  },

  onDeviceReady: function() {
    var txtLocation = document.getElementById("location");
	var txtTakePicture = document.getElementById("takePicture");
    var btTakePicture = document.getElementById("take-picture");
    var btnMapa = document.getElementById("btnMapa");

    btTakePicture.addEventListener("click", takePicture, false);
    btnMapa.addEventListener("click", viewMap, false);

    txtLocation.innerHTML = "Aguarde enquanto verificamos sua localização...";

    //popular latitude e longitude
    getCurrentPosition(function(latitude, longitude) {
      txtLocation.innerHTML = " ";
	  txtTakePicture.innerHTML = "Tire uma foto!";
      btTakePicture.disabled = false;
    });

    manageDatabase();

    imageSlider.init("image-slider", {
      mode: "horizontal",
      onSlideAfter: function (slideElement, oldIndex, newIndex) {
        getImageDB(newIndex, function(photo) {
          imageSlider.currentPhoto = photo;
          updatePhotoData(imageSlider);
        });
      },
      onSliderLoad: function(currentIndex) {
        getImageDB(currentIndex, function(photo) {
          imageSlider.currentPhoto = photo;
          updatePhotoData(imageSlider);
        });
      }
    });

    imageSlider.reload(addAllImagesToSlider);
  }

};

app.initialize();
